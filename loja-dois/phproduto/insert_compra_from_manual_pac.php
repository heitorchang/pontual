<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

// print_r($obj);
$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("insert into compra (data, codigo, qtde) values (:data, :codigo, :qtde)");

$dbh->beginTransaction();

echo $lastRow;
echo "\n";

for ($r = 1; $r <= $lastRow; $r++) {
    if (isset($sheet[$r . '-2'])) {
        $compraData = $sheet[$r . '-2'];
        $compraCodigo = $sheet[$r . '-3'];
        $compraQtde = $sheet[$r . '-4'];

        $compraISO = dmyToISO($compraData);
        
        echo "Attempt to insert $compraData $compraCodigo $compraQtde\n";

        try {
            $stmt->execute([':data' => $compraISO,
                            ':codigo' => $compraCodigo,
                            ':qtde' => $compraQtde]);
        } catch (Exception $e) {
            echo $e;
        }
        
        ob_flush();
    }
}

$dbh->commit();

addTimestamp($dbh, "insertCompra");
