<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

// print_r($obj);
$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("insert into compra (data, container, codigo, qtde) values (:data, :container, :codigo, :qtde)");

$dbh->beginTransaction();

echo $lastRow;
echo "\n";

$compraData = $sheet['5' . '-18'];
$compraContainer = $sheet['1' . '-15'];

// NOTE: Compra Data begins in row 7
for ($r = 7; $r <= $lastRow; $r++) {
    if (isset($sheet[$r . '-2'])) {
        $compraCodigo = $sheet[$r . '-2'];
        $compraQtde = $sheet[$r . '-13'];

        $compraISO = dmyToISO($compraData);
        
        echo "Attempt to insert $compraContainer $compraData $compraCodigo $compraQtde\n";

        try {
          $stmt->execute([':data' => $compraISO,
                          ':container' => $compraContainer,
                          ':codigo' => $compraCodigo,
                          ':qtde' => $compraQtde]);
        } catch (Exception $e) {
          echo $e;
          echo "\n";
        }
        
        ob_flush();
    }
}

$dbh->commit();

addTimestamp($dbh, "insertCompra");
