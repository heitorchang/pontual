<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

// print_r($obj);
$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("update produto set disp = :disp, resv = :resv where codigo = :codigo");

$site_update_stmt = $dbh->prepare("update site_produtos_estoque_update set updated = :updated where codigo = :codigo");

$dbh->beginTransaction();

echo $lastRow . "\n";

for ($r = 1; $r <= $lastRow; $r++) {
  if (isset($sheet[$r . '-2'])) {
    $produto_nome = $sheet[$r . '-2'];

    $rowCodigo = $sheet[$r . '-1'];
    $rowNome = $sheet[$r . '-2']; 
    $rowDisp = $sheet[$r . '-6'];
    $rowResv = $sheet[$r . '-9'];

    echo "Attempt to update $rowCodigo " . $produto_nome . "\n";
    ob_flush();
    
    // updateProdutoStock($dbh, $rowCodigo, $rowDisp, $rowResv);
    try {
      $stmt->execute([':disp' => $rowDisp,
                      ':resv' => $rowResv,
                      ':codigo' => $rowCodigo]);
    } catch (Exception $e) {
      echo $e;
    }

    // update table "site_produtos_estoque_update"
    // it is the only list of products that really matter
    // we need to keep track of updates because Loja virtual
    // conferencia de estoque does not properly return every item

    try {
      $site_update_stmt->execute([':updated' => date("Y-m-d"),
                                  ':codigo' => $rowCodigo]);
    } catch (Exception $e) {
      echo $e;
    }
  }
}

$dbh->commit();

addTimestamp($dbh, "updateProdutoStock");

echo "\n";
