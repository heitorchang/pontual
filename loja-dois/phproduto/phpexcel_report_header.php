<?php
require_once("PHPExcel/PHPExcel.php");

require_once('PHPExcel/PHPExcel/Writer/Excel2007.php');

$objPHPExcel = new PHPExcel();

$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->setTitle(date("d-m-Y"));
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader("Reposição " . date("d/m/Y"));

// page numbers
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('Pagina &P de &N');
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter('Pagina &P de &N');

function writeCell($obj, $ref, $val) {
    $obj->getActiveSheet()->setCellValueExplicit($ref, $val, PHPExcel_Cell_DataType::TYPE_STRING);
}

function writeBoldCell($obj, $ref, $val) {
    writeCell($obj, $ref, $val);
    setBold($obj, $ref);
}

function writeBoldAndCenterCell($obj, $ref, $val) {
    writeCell($obj, $ref, $val);
    setBold($obj, $ref);
    setCenter($obj, $ref);
}

function writeCenterCell($obj, $ref, $val) {
    writeCell($obj, $ref, $val);
    setCenter($obj, $ref);
}

function setBold($obj, $ref) {
    $obj->getActiveSheet()->getStyle($ref)->getFont()->setBold(true);
}

function setCenter($obj, $ref) {
    $ctr = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
    $obj->getActiveSheet()->getStyle($ref)->applyFromArray($ctr);
}

writeBoldCell($objPHPExcel, "A1", "Código");
writeBoldAndCenterCell($objPHPExcel, 'B1', "Disponível");
writeBoldAndCenterCell($objPHPExcel, "C1", "Reservado");
writeBoldAndCenterCell($objPHPExcel, "D1", "Chegando");
writeBoldAndCenterCell($objPHPExcel, "E1", "Vendas 2017");
writeBoldAndCenterCell($objPHPExcel, "F1", "Vendas 2018");
writeBoldAndCenterCell($objPHPExcel, "G1", "Caixa");

/*
writeBoldCell($objPHPExcel, "A2", "137350RR");
writeBoldAndCenterCell($objPHPExcel, "B2", "12.342");
writeCell($objPHPExcel, "A3", "Chaveiro de metal Redondo sem embalagem");
*/

// Post Processing
// set column widths
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(7);

$objPHPExcel->getActiveSheet()->setPrintGridlines(true);
$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);

