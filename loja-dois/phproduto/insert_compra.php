<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

// print_r($obj);
$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("insert into compra (data, codigo, qtde) values (:data, :codigo, :qtde)");

$dbh->beginTransaction();

echo $lastRow;
echo "\n";

$compraData = $sheet['18' . '-6'];
$compraContainer = $sheet['15' . '-1'];

// NOTE: Compra Data begins in row 7
for ($r = 7; $r <= $lastRow; $r++) {
    if (isset($sheet[$r . '-2'])) {
        $compraCodigo = $sheet[$r . '-2'];
        $compraQtde = $sheet[$r . '-13'];

        $compraISO = dmyToISO($compraData);
        
        echo "Attempt to insert $compraContainer $compraData $compraCodigo $compraQtde\n";

        try {
            $stmt->execute([':data' => $compraISO,
                            ':codigo' => $compraCodigo,
                            ':qtde' => $compraQtde]);
        } catch (Exception $e) {
            echo $e;
        }
        
        ob_flush();
    }
}

$dbh->commit();

addTimestamp($dbh, "insertCompra");
