<?php

require_once("../db.php");
require_once("config.php");

$vendas_ultimos_12_meses = 'select produto.codigo, sum(venda.qtde) as vendeu
from produto, venda, pedido
where venda.codigo = produto.codigo
and pedido.data > current_date() - interval 12 month
and venda.numero = pedido.numero
group by produto.codigo';

$pac = 'select codigo, sum(qtde) as chegando
from pac
group by codigo';

$estoque = 'select codigo, (disp + resv) as estoque from produto';

// base list should be vendas. Sometimes a codigo does not show up in conferencia, in which case it should be counted as 0

$long_list = [];

$produtos_snapshot = [];
$is_excl = [];  // has exclamation point

$vendas_rows = $dbh->query($vendas_ultimos_12_meses);
foreach ($vendas_rows as $row) {
    $produtos_snapshot[$row["codigo"]]["vendas"] = $row["vendeu"];
}

$pac_rows = $dbh->query($pac);
foreach ($pac_rows as $row) {
    $produtos_snapshot[$row["codigo"]]["pac"] = $row["chegando"];
}
           
$estoque_rows = $dbh->query($estoque);
foreach ($estoque_rows as $row) {
    $long_list[] = $row["codigo"];
    $produtos_snapshot[$row["codigo"]]["estoque"] = $row["estoque"];
}


function include_codigo($codigo, $snapshot, &$is_excl) {
    $codigo = strtoupper(trim($codigo));
    // always include codigo with !
    if (substr($codigo, -1) == "!") {
        echo "including " . substr($codigo, 0, strlen($codigo) - 1) . " because of Exclamation mark (!)\n";
        $is_excl[substr($codigo, 0, strlen($codigo) - 1)] = "(!)";
        return substr($codigo, 0, strlen($codigo) - 1);
    }
    return is_estoque_low($codigo, $snapshot);
}

function is_estoque_low($codigo, $snapshot) {
  global $_MONTHS_BACK;
  
    if (isset($snapshot[$codigo]["pac"])) {
        if ($snapshot[$codigo]["pac"] == 0) {
            // ignore this because of special marker in N:\CHEGANDO
            echo "skipping $codigo because of 0 (ignore) in Chegando\n";
            return false;
        }
    } else {
        // set to zero after checking for special CHEGANDO marker
        $snapshot[$codigo]["pac"] = 0;
    }

    $count_total_stock = $snapshot[$codigo]["estoque"];
    $count_chegando = $snapshot[$codigo]["pac"];
  if (isset($snapshot[$codigo]["vendas"])) {
    // $five_mos = (int) (5 * ($snapshot[$codigo]["vendas"] / 12));
    
    $sales_n_months_back = (int) ($_MONTHS_BACK * ($snapshot[$codigo]["vendas"] / 12));
  } else {
    // $five_mos = 0;
    
    $sales_n_months_back = 0;
  }

  // if (($count_total_stock + $count_chegando) < $five_mos) {
  if (($count_total_stock + $count_chegando) < $sales_n_months_back) {
    echo "including $codigo because stock is low\n";
    return $codigo;
  }
    
  // echo "skipping $codigo because stock is sufficient: $count_total_stock + $count_chegando (PAC) is more than five months: $five_mos\n";
  echo "skipping $codigo because stock is sufficient: $count_total_stock + $count_chegando (PAC) is more than $_MONTHS_BACK months: $sales_n_months_back\n";
  
  return false;
}

?>
<pre>
<?php


foreach (explode("\n", trim($_POST["lista_codigos"])) as $input_cod) {
    $input_cod = trim($input_cod);
    $long_list = array_merge($long_list, add_letras($input_cod, $dbh));
}

$long_list = array_unique($long_list);
asort($long_list);

$filter_first_pass = [];

foreach ($long_list as $input_cod) {
    if (!isset($is_excl[$input_cod])) {
        $is_excl[$input_cod] = "";
    }
    
    $output_codigo = include_codigo(strtoupper($input_cod), $produtos_snapshot, $is_excl);
    if ($output_codigo) {
        // echo "including $output_codigo\n";
        $filter_first_pass[] = $output_codigo;
    } else {
        // echo "skipping $input_cod\n";
    }
}

function add_letras($codigo, $dbh) {
    if (substr($codigo, -1) == "!") {
        // return [substr($codigo, 0, strlen($codigo) - 1)];
        return [$codigo];
    }
    $stmt = $dbh->prepare('select codigo from produto where codigo like :codigo');
    $cod = "%$codigo%";
    $stmt->bindParam(":codigo", $cod, PDO::PARAM_STR);
    $stmt->execute();

    $result = [];
    foreach ($stmt as $row) {
        $result[] = $row["codigo"];
    }
    return $result;
}

$filter_first_pass = array_unique($filter_first_pass);
asort($filter_first_pass);

