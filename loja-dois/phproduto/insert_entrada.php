<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

// print_r($obj);
$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("insert into entrada (nome, data) values (:nome, :data)");

$dbh->beginTransaction();

echo $lastRow;
echo "\n";

for ($r = 1; $r <= $lastRow; $r++) {
    if (isset($sheet[$r . '-2'])) {
        $entradaNome = $sheet[$r . '-1'];
        $entradaDataFormatada = $sheet[$r . '-2'];

        $entradaISO = dmyToISO($entradaDataFormatada);

        echo "Attempt to insert $entradaNome\n";

        try {
            $stmt->execute([':nome' => $entradaNome,
                            ':data' => $entradaISO]);
        } catch (Exception $e) {
            echo $e;
        }
        
        ob_flush();
    }
}

$dbh->commit();

addTimestamp($dbh, "insertEntrada");
