<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

// print_r($obj);
$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("update produto set caixa = :caixa where codigo = :codigo");

$dbh->beginTransaction();

echo $lastRow;
echo "\n";

for ($r = 1; $r <= $lastRow; $r++) {
    if (isset($sheet[$r . '-1'])) {
        $produto_codigo = $sheet[$r . '-1'];
        if ($produto_codigo != "Código") {
            echo "Attempt to update " . $produto_codigo . "\n";
            ob_flush();
            
            $rowCodigo = $sheet[$r . '-1'];
            $rowCaixa = $sheet[$r . '-12'];
            try {
                $stmt->execute([':caixa' => $rowCaixa,
                                ':codigo' => $rowCodigo]);
                // updateProdutoCaixa($dbh, $rowCodigo, $rowCaixa);
            } catch (Exception $e) {
                echo $e;
            }
        }
    }
}

$dbh->commit();

addTimestamp($dbh, "updateProdutoCaixa");

