<?php
require_once("../db.php");

$json = file_get_contents("php://input");
$sheet = json_decode($json, true);

$lastRow = (int) $sheet['lastRow'];

$stmt = $dbh->prepare("insert into pedido (numero, data, cliente) values (:numero, :data, :cliente)");

$dbh->beginTransaction();

echo $lastRow;
echo "\n";

for ($r = 1; $r <= $lastRow; $r++) {
    if (isset($sheet[$r . '-2']) && isset($sheet[$r . '-1'])) {
        $pedidoNumero = $sheet[$r . '-2'];
        $pedidoCliente = $sheet[$r . '-3'];
        $pedidoDataFormatada = $sheet[$r . '-6'];

        $pedidoISO = dmyToISO($pedidoDataFormatada);

        echo "Attempt to insert $pedidoNumero $pedidoCliente\n";

        try {
            $stmt->execute([':numero' => $pedidoNumero,
                            ':data' => $pedidoISO,
                            ':cliente' => $pedidoCliente]);
        } catch (Exception $e) {
            echo $e;
        }
        
        ob_flush();
        // insertProduto($dbh, $rowCodigo, $rowNome, $rowDisp, $rowResv, 0);
    }
}

$dbh->commit();

addTimestamp($dbh, "insertPedido");
