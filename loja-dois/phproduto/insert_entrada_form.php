<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>XLSX to Array Demo</title>
  </head>
  <body>
     <?php
     require_once("../db.php");
include_once("header.php");
echo "Last Insert Entrada: ";
echo getTimestamp($dbh, "insertEntrada");
?>

<br><br>
Containers (Movimento > Precos > Lista precos - Container)<br>
Arquivo em D:\Pontual\Containers\Containers_DATA.xls<br>
<br>
Format: (note the date is entered as Text, with a quote)<br> 
CCTC/1512 ; '10/09/15<br>
CCTC/1511 ; '20/08/15 etc.<br>
     <input type="file" id="fileInput">

    <pre id="out">
    </pre>
    <script src="lib/js/xlsx.core.min.js"></script>
    <script src="lib/js/jquery-2.1.4.min.js"></script>
    <script src="xlsx-to-array.js"></script>
    <script src="insert_entrada.js"></script>
  </body>
</html>
