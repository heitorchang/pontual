<?php
// NOTE: Loja Virtual xls unreadable by PHPExcel. Must Save as ODS or XLSX

$target_file = "uploads/" . basename($_FILES["uploadedFile"]["name"]);

if (move_uploaded_file($_FILES["uploadedFile"]["tmp_name"], $target_file)) {
    echo "File $target_file uploaded.";
} else {
    echo "Error uploading file.";
}

