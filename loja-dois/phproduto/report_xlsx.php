<?php

require_once("../db.php");
require_once("config.php");
require_once("phpexcel_report_header.php");

mb_internal_encoding("UTF-8");

// $cods = ["143192P",
//         "143129"];

$cods = $_POST['selected_cod'];

$output = "Código,Estoque disponível,Estoque reservado,Chegando (n. container),Vendas 2017,Vendas 2018,Cx. grande\n";

require_once("sqls_for_report.php");

$elts = [];

// Is out of stock at this point in time
$is_out = false;

$xlsxrow = 2;
foreach ($cods as $cod) {
    $elts[$cod] = [];

    $is_out = false;

    try {
        // include codigo to incluido, marking today's date
        $stmt = $dbh->prepare("insert into incluido (codigo, data) values (:codigo, current_date())");
        $stmt->execute([':codigo' => $cod]);
    } catch (Exception $e) {

    }
    
    $stmt = $dbh->prepare($pac);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    if (empty(count($row["pac_text"]))) {
        $elts[$cod]["pac"] = 0;
    } else {
        $elts[$cod]["pac"] = $row["pac_text"];
    }

    /* leave filtering to filter.php
    if (substr($elts[$cod]["pac"], 0, 3) === "0 (") {
        continue;
    }
    */
    
    $stmt = $dbh->prepare($prodinfo);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["codigo"] = $cod;
    $elts[$cod]["disp"] = $row["disp"];
    $elts[$cod]["resv"] = $row["resv"];
    $elts[$cod]["caixa"] = $row["caixa"];
    $elts[$cod]["nome"] = uc_first_only($row["nome"]);

    try {
        // delete existing row
        $stmt = $dbh->prepare("delete from ultimo_estoque where codigo = :codigo");
        $stmt->execute([':codigo' => $cod]);

        // get raw disp/resv
        $stmt_raw_disp = $dbh->prepare($prodraw);
        $stmt_raw_disp->execute([':codigo' => $cod]);
        $row_raw = $stmt_raw_disp->fetch();
                
        // include current stock to be viewed later
        $stmt = $dbh->prepare("insert into ultimo_estoque (codigo, estoque) values (:codigo, :estoque)");
        $stmt->execute([':codigo' => $cod,
                        ':estoque' => intval($row_raw["disp"]) + intval($row_raw["resv"])]);
    } catch (Exception $e) {
        die($e->getMessage());
    }
            
    if ($row["caixa"] == 0) {
        // echo "<br><br><font color='red'>WARNING: Zero caixa for cod. $cod</font><br><br>";
        $output .= "\nWARNING: Zero caixa for cod. $cod\n\n";
    }

    $stmt = $dbh->prepare($vendas);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["vendas2017"] = $row["vendas2017"];
    $elts[$cod]["vendas2018"] = $row["vendas2018"];

    if (empty($elts[$cod]["vendas2017"])) {
        $elts[$cod]["vendas2017"] = 0;
    }
    if (empty($elts[$cod]["vendas2018"])) {
        $elts[$cod]["vendas2018"] = 0;
    }

    $stmt = $dbh->prepare($ultimo);
    $stmt->execute([':codigo1' => $cod]);
  // ':codigo2' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["ultimo"] = $row["ultimo"];
    if (empty($elts[$cod]["ultimo"])) {
        $elts[$cod]["ultimo"] = "";
    } else {
        $elts[$cod]["ultimo"] .= "\n";
    }

  // Ultimo container calculation is incorrect, search manually
  // $elts[$cod]["ultimo"] = "Último container - ";  
  
    $stmt = $dbh->prepare($largest);
    $stmt->execute([':codigo' => $cod]);
    $rows = $stmt->fetchAll();

    $largest_vendas = array_column($rows, "venda");
  // $elts[$cod]["largest"] = implode("\n", array_map("uc_first_lower", $largest_vendas));
  $elts[$cod]["largest"] = implode("\n", $largest_vendas);

    $stmt = $dbh->prepare($curstock);
    $stmt->execute([':codigo' => $cod]);
    $row = $stmt->fetch();

    $elts[$cod]["curstock"] = $row["curstock"];
    
    $stmt = $dbh->prepare($stockhist);
    $stmt->execute([':codigo1' => $cod,
                    ':codigo2' => $cod,
                    ':codigo3' => $cod]);
    $rows = $stmt->fetchAll();

    $outofstock_lines = "";
    $outofstock_array = [];
    
    foreach ($rows as $row) {
        // threshold for being out of stock.
        $threshold = $_CONFIG_THRESHOLD;

        // echo $row['d'] . ' ' . $row['q'];
        $elts[$cod]["curstock"] -= $row['q'];
        
        // echo ' ' . $elts[$cod]["curstock"];

        if ($is_out) {
            if ($elts[$cod]["curstock"] > $threshold) {
                $is_out = false;
                $display_date = ISOToDmy($current_end_date);
                if ($display_date == date("d/m/y")) {
                    $display_date = "agora";
                }
                
                $outofstock_lines .= "Sem estoque de " . ISOToDmy($row['d']) .
                    " até " . $display_date . "\n";
                $outofstock_array[] = "Sem estoque de " . ISOToDmy($row['d']) .
                    " até " . $display_date . "\n";
            }
        }
        
        if ($elts[$cod]["curstock"] < $threshold && !$is_out) {
            $is_out = true;
            // echo " OUT OF STOCK HERE";
            $current_end_date = $row['d'];
        }
        // echo "\n";
    }

    $elts[$cod]["outofstock"] = $outofstock_lines;

    // prod info
    writeBoldCell($objPHPExcel, "A$xlsxrow", $cod);
    writeBoldAndCenterCell($objPHPExcel, "B$xlsxrow", $elts[$cod]['disp']);
    writeBoldAndCenterCell($objPHPExcel, "C$xlsxrow", $elts[$cod]['resv']);
    writeBoldAndCenterCell($objPHPExcel, "D$xlsxrow", $elts[$cod]['pac']);
    writeBoldAndCenterCell($objPHPExcel, "E$xlsxrow", $elts[$cod]['vendas2017']);
    writeBoldAndCenterCell($objPHPExcel, "F$xlsxrow", $elts[$cod]['vendas2018']);
    writeBoldAndCenterCell($objPHPExcel, "G$xlsxrow", $elts[$cod]['caixa']);
    $xlsxrow++;

    // nome
    writeCell($objPHPExcel, "A$xlsxrow", $elts[$cod]['nome']);
    $xlsxrow++;
    
    // ultimo
    writeCell($objPHPExcel, "A$xlsxrow", trim($elts[$cod]['ultimo']));
    $xlsxrow++;

    // out of stock (ARRAY)
    foreach ($outofstock_array as $outofstock_line) {
        writeCell($objPHPExcel, "A$xlsxrow", trim($outofstock_line));
        $xlsxrow++;
    }
    
    // largest
    foreach ($largest_vendas as $largest_line) {
      // writeCell($objPHPExcel, "A$xlsxrow", trim(uc_first_lower($largest_line)));
      writeCell($objPHPExcel, "A$xlsxrow", trim($largest_line));
        $xlsxrow++;
    }
    $xlsxrow++;
}

function report_elt($elts, $codigo) {
    return <<<_END
$codigo,"{$elts[$codigo]['disp']}","{$elts[$codigo]['resv']}",{$elts[$codigo]['pac']},"{$elts[$codigo]['vendas2017']}","{$elts[$codigo]['vendas2018']}","{$elts[$codigo]['caixa']}"
{$elts[$codigo]['nome']}
{$elts[$codigo]['ultimo']}{$elts[$codigo]['outofstock']}{$elts[$codigo]['largest']}


_END;
}

function uc_first_lower($s) {
    return ucwords(mb_strtolower($s));
}

function uc_first_only($s) {
    return ucfirst(mb_strtolower($s));
}

header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-Disposition: attachment; filename="cardex_report.xlsx"');

$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save("php://output");
