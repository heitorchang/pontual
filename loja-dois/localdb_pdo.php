<?php
$db_settings = [
    'host' => '127.0.0.1',
    'port' => '3306',
    'name' => 'lojadois',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    ];

try {
    $dbh = new PDO(
        sprintf(
            'mysql:host=%s;dbname=%s;port=%s;charset=%s',
            $db_settings['host'],
            $db_settings['name'],
            $db_settings['port'],
            $db_settings['charset']
            ),
        $db_settings['username'],
        $db_settings['password'],
        
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
} catch (PDOException $e) {
    print "Error: " . $e->getMessage();
    die();
}

// produto

// ERROR: casting will produce "cannot pass by reference" error
function insertProduto($dbh, $codigo, $nome, $disp, $resv, $caixa) {
    $stmt = $dbh->prepare("insert into produto (codigo, nome, disp, resv, caixa) values (:codigo, :nome, :disp, :resv, :caixa)");
    $stmt->bindParam(':codigo', $codigo);
    $stmt->bindParam(':nome', $nome);
    $stmt->bindParam(':disp', $disp);
    $stmt->bindParam(':resv', $resv);
    $stmt->bindParam(':caixa', $caixa);
    try {
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
}

function updateProdutoStock($dbh, $codigo, $disp, $resv) {
    $stmt = $dbh->prepare("update produto set disp = :disp, resv = :resv where codigo = :codigo");
    $stmt->bindParam(':codigo', $codigo);
    $stmt->bindParam(':disp', $disp);
    $stmt->bindParam(':resv', $resv);
    try {
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
}

function updateProdutoCaixa($dbh, $codigo, $caixa) {
    $stmt = $dbh->prepare("update produto set caixa = :caixa where codigo = :codigo");
    $stmt->bindParam(':codigo', $codigo);
    $stmt->bindParam(':caixa', $caixa);
    try {
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
}

function addTimestamp($dbh, $datatype) {
    $stmt = $dbh->prepare("insert into last_update (datatype) values (:datatype)");
    $stmt->bindParam(":datatype", $datatype);
    try {
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
}

function getTimestamp($dbh, $datatype) {
    $stmt = $dbh->prepare("select max(updated) from last_update where datatype = :datatype");
    $stmt->bindParam(":datatype", $datatype);
    
    $stmt->execute();
    return $stmt->fetch()["max(updated)"];
}

function dmyToEpoch($dmy) {
    $elts = explode("/", $dmy);
    return date('U', strtotime("$elts[1]/$elts[0]/$elts[2]"));
}

function dmyToISO($dmy) {
    $elts = explode("/", $dmy);
    return date('Y-m-d', strtotime("$elts[1]/$elts[0]/$elts[2]"));
}

function epochToDmy($epoch) {
    return date('d/m/y', $epoch);
}

function ISOToDmy($isodate) {
    $elts = explode("-", $isodate);
    return date('d/m/y', strtotime("$elts[1]/$elts[2]/$elts[0]"));
}
