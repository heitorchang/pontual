Administração do site da Pontual

Uma coleção de scripts PHP que lêem dados armazenados por MySQL.

O banco de dados deve ser criado manualmente através das ferramentas da hospedagem do site.

Defina o usuário e senha do banco de dados em ../../kitweb_v2_db_production.php (arquivo acima do diretório public_html)

O design simples é possivelmente vulnerável, use com cuidado.

Atualmente o diretório dos scripts requer uma senha configurada com .htaccess e .htpasswd. 
