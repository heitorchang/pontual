Merchant

Adaptation of Loja Dois

Create a dummy Django project and copy its settings.py to merchant/merchant

Then add to merchant/settings.py:

# Application definition

INSTALLED_APPS = [
    'records.apps.RecordsConfig',
    'reposicao.apps.ReposicaoConfig',
    'django.contrib.admin',
    # ...


* Data upload order

1. Insert Produto and Caixa - Movimento > Lista de Precos > Lista Geral
2. Update Stock - Produtos > Conferencia de Estoque > Ativo AND Inativo
3. Insert Pedido - Vendas por numero > Sintetico
4. Insert Venda - Vendas por produto > Analitico
5. Insert Compra - PAC section > Imprimir > Save as Excel
6. Update Chegando - Save CHEGANDO as .xls
       Format:
       Codigo ; Cod (texto)     ; Container ; Qtde
       140310 ; =ESQUERDA(A1;8) ; 1710      ; 7200
7. Enter Codigos


* Reading and Writing Excel files

pip install openpyxl  # version 2.4.8
pip install xlrd      # version 1.1.0

Documentation:
http://openpyxl.readthedocs.io/en/default/
http://xlrd.readthedocs.io/en/latest/


* Models for records

Produto - Unique codigo
_______
codigo - str - Unique
nome - str - blank=True
caixa - int - default 0
estoque_disp - int - default 0 
estoque_resv - int - default 0
preco - Decimal(10, 4) - default 0.0
estoque_last_updated - date - blank=True


Vendedor
________
nome - str


Cliente - Unique nome
_______
codigo - str - blank=True
nome - str
vendedor - fk
endereco - str - blank=True
bairro - str - blank=True
cidade - str - blank=True
estado - str - blank=True
cep - str - blank=True
telefone - str - blank=True
celular - str - blank=True
email - str - blank=True
cnpj - str - blank=True
inscricao - str - blank=True


Chegando - Unique container + produto
  class Meta:  unique_together = ('produto', 'nome')
________
produto - fk
qtde - int - default 0
nome - str


Pedido - Unique numero
______
numero - int - Unique
cliente - fk
data - date
desconto - Decimal(5, 4)


ItemPedido - Unique pedido + produto
___________
pedido - fk
produto - fk
qtde - int - default 0


Compra - Unique container
______ 
container - str - Unique
data - date


ItemCompra - Unique compra + produto
___________
compra - fk
produto - fk
qtde - int - default 0


PedidoUniao - Unique numero
____________
numero - int - Unique
cliente - fk
data - date
desconto - Decimal(5, 4)


ItemPedidoUniao - Unique pedidoUniao + produto
_________________
pedido_uniao - fk
produto - fk
qtde - int - default 0


* Models for reposicao

Incluido
________
produto - fk
data - date


Atualizado
__________
tipo - str
data - date


UltimoEstoque - Update if exists
______________
produto - fk
estoque - int - default 0

