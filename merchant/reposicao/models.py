from django.db import models
from records.models import Produto

class Incluido(models.Model):
    produto = models.ForeignKey(Produto, null=True, on_delete=models.SET_NULL)
    data = models.DateField()

    def __str__(self):
        return "{} {}".format(self.data, self.produto.codigo)

class Atualizado(models.Model):
    tipo = models.CharField(max_length=32)
    data = models.DateField()

    def __str__(self):
        return "{} {}".format(self.data, self.tipo)

class UltimoEstoque(models.Model):
    produto = models.ForeignKey(Produto, null=True, on_delete=models.SET_NULL)
    estoque = models.IntegerField(default=0)

    def __str__(self):
        return "{} pçs {}".format(self.estoque, self.produto.codigo)
