from django import forms

class PreliminaryReportForm(forms.Form):
    codigos = forms.CharField(widget=forms.Textarea)

class MatchUniaoForm(forms.Form):
    codigo = forms.CharField()
    data = forms.CharField()
    qtde = forms.CharField()

class UnionizeForm(forms.Form):
    file = forms.FileField()
