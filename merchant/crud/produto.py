from django.db import IntegrityError
from records.models import Produto

# Use only as a reference, it will be confusing to use custom names
# for what amounts to a few lines of code

def create(codigo, nome, caixa, preco):
    try:
        produto = Produto(codigo=codigo, nome=nome, caixa=caixa, preco=preco)
        produto.save()
        return "{} saved".format(codigo)
    except IntegrityError:
        raise ValueError("{} already exists.".format(codigo))

def read(codigo):
    try:
        return Produto.objects.get(codigo=codigo)
    except Produto.DoesNotExist:
        return None
    
def update(codigo, **fields):
    # https://docs.djangoproject.com/en/1.11/ref/models/instances/
    # get() exists to retrieve a single instance, but even in
    # the example, filter() is used with a pk, which implies
    # only one object will be returned, because a pk is unique
    #
    # MyModel.objects.filter(pk=obj.pk).update(val=F('val') + 1)
    
    Produto.objects.filter(codigo=codigo).update(**fields)
    return "{} updated".format(codigo)

def delete(codigo):
    try:
        read(codigo).delete()
        return "{} deleted".format(codigo)
    except AttributeError:
        return "{} not found".format(codigo)
    
