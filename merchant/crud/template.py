from django.db import IntegrityError
from records.models import CLASS

def create():
    try:
        obj = CLASS()
        obj.save()
        return "{} saved".format("")
    except IntegrityError:
        return "{} already exists.".format("")

def read():
    try:
        return CLASS.objects.get()
    except CLASS.DoesNotExist:
        return None
    
def update(, **fields):
    try:
        CLASS.objects.filter().update(**fields)
        return "{} updated".format()
    except:
        return "Error updating {}".format()

def delete():
    obj = read()
    if obj:
        obj.delete()
        return "{} deleted".format()
    return "Error deleting {}".format()
