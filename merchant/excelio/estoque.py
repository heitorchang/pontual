import re
import xlrd
from datetime import date

from django.db import IntegrityError, transaction

from decimal import Decimal
from records.models import Produto, Atualizado

@transaction.atomic
def update(f):
    """Update produtos from UploadedFile f"""
    response = ""
    response_err = ""
    
    book = xlrd.open_workbook(file_contents=f.read())
    sheet = book.sheet_by_index(0)

    rows = sheet.nrows
    
    # columns
    CODIGO = 0
    # NOME = 1  # not needed
    DISP = 5
    RESV = 8

    # valid_codigo_pat = re.compile(r'\d{6}[A-Z]{0,2}$')
    
    valid_codigo_pat = re.compile(r'\w{5,8}$')
    
    today = date.today().strftime("%Y-%m-%d")
    
    response += "<pre>"
    response_err += "<pre style='color: red;'>"
    
    for ROW in range(rows):
        codigoCellValue = sheet.cell(ROW, CODIGO).value
        if isinstance(codigoCellValue, str):
            codigo = codigoCellValue
        else:
            codigo = str(int(codigoCellValue))

        if codigo.strip() == "":
            continue
            
        # check if codigo is valid
        if not valid_codigo_pat.match(codigo):
            response_err += "{} is not a valid codigo\n".format(codigo[:30])
            continue

        try:
            disp = int(sheet.cell(ROW, DISP).value)
            resv = int(sheet.cell(ROW, RESV).value)
        except ValueError:
            response_err += "Row {}: Invalid disponivel and reservado values\n".format(ROW)
            continue

        # update codigo
        try:
            updated = Produto.objects.filter(codigo=codigo).update(estoque_disp=disp, estoque_resv=resv, estoque_last_updated=today)
            if not updated:
                response_err += "{} not found, skipping\n".format(codigo)
            else:
                response += "{} updated\n".format(codigo)
            
        except IntegrityError:
            response_err += "IntegrityError in adding or updating {} \n".format(codigo)
    else:
        response += "ALL OK\n"
        Atualizado.atualizar('estoque')
        
    return response_err + "</pre>" + response
