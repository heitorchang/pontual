import re
import xlrd
from datetime import date, datetime

from django.db import IntegrityError, transaction

from records.models import Chegando, Produto, Atualizado

@transaction.atomic
def create(f):
    """Create Chegando from UploadedFile f"""
    response = ""
    response_err = ""
    
    book = xlrd.open_workbook(file_contents=f.read())
    sheet = book.sheet_by_index(0)

    rows = sheet.nrows
    
    # columns
    CODIGO = 1
    NOME = 2
    QTDE = 3

    response += "<pre>"
    response_err += "<pre style='color: red;'>"

    # delete all
    Chegando.objects.all().delete()

    valid_codigo_pat = re.compile(r'\w{5,8}$')

    for ROW in range(1, rows):
        codigoCellValue = sheet.cell(ROW, CODIGO).value
        if isinstance(codigoCellValue, str):
            codigo = codigoCellValue
        else:
            codigo = str(int(codigoCellValue))

        codigo = codigo.strip()
        
        # check if codigo is valid
        if codigo == "" or codigo.lower() == "codigo":
            continue
            
        if not valid_codigo_pat.match(codigo):
            response_err += "{} is not a valid codigo\n".format(codigo)
            continue

        try:
            produto = Produto.objects.get(codigo=codigo)
        except:
            # Create a temporary product
            response_err += "Could not find produto {}, creating a temporary produto\n".format(codigo)
            p = Produto(codigo=codigo, nome='SEM NOME')
            p.save()

        nomeRaw = sheet.cell(ROW, NOME).value
        if isinstance(nomeRaw, str):
            nome = nomeRaw
        else:
            nome = str(int(nomeRaw))
        
        try:
            qtde = int(sheet.cell(ROW, QTDE).value)
        except ValueError:
            response_err += "Could not read codigo {}'s qtde. Skipping\n".format(codigo)
            continue
        
        # create Chegando
        try:
            c = Chegando(produto=produto, qtde=qtde, nome=nome)
            c.save()
            response += "{} {} saved\n".format(nome, codigo)
            
        except:
            response_err += "IntegrityError in adding or updating {}. Aborting\n".format(codigo)
            break
    else:
        response += "ALL OK\n"
        Atualizado.objects.get(tipo='chegando').atualizarAgora()
        
    return response_err + "</pre>" + response


