import re
import xlrd
from datetime import date, datetime

from django.db import IntegrityError, transaction

from records.models import Pedido, Produto, ItemPedido, Atualizado

@transaction.atomic
def create(f):
    """Create ItemPedidos from UploadedFile f"""
    response = ""
    response_err = ""
    
    book = xlrd.open_workbook(file_contents=f.read())
    sheet = book.sheet_by_index(0)

    rows = sheet.nrows
    
    # columns
    NUMERO = 1
    CODIGO_PRODUTO = 2
    QTDE = 7

    response += "<pre>"
    response_err += "<pre style='color: red;'>"

    for ROW in range(rows):
        numeroCellValue = sheet.cell(ROW, NUMERO).value

        if isinstance(numeroCellValue, str):
            continue

        try:
            numero = int(numeroCellValue)
        except ValueError:
            response_err += "Could not read numero {}. Skipping\n".format(numeroCellValue)
            continue
        
        if numero < 50000:
            response_err += "Numero {} too small. Skipping\n".format(numero)
            continue

        try:
            pedido = Pedido.objects.get(numero=numero)
            response += "Processing pedido {}\n".format(numero)
        except Pedido.DoesNotExist:
            response_err += "Could not find pedido {}, aborting\n".format(numero)
            break

        produtoCellValue = sheet.cell(ROW, CODIGO_PRODUTO).value
        if isinstance(produtoCellValue, str):
            codigoProduto = produtoCellValue
        else:
            codigoProduto = str(int(produtoCellValue))

        if codigoProduto == "DESC":
            continue
        
        try:
            produto = Produto.objects.get(codigo=codigoProduto)
        except Produto.DoesNotExist:
            response_err += "Could not find produto {}, aborting\n".format(codigoProduto)
            break

        qtdeCellValue = sheet.cell(ROW, QTDE).value
        if isinstance(qtdeCellValue, str):
            continue
        qtde = int(qtdeCellValue)
        
        # create itemPedido
        try:
            item, itemCreated = ItemPedido.objects.update_or_create(pedido=pedido, produto=produto, defaults={'qtde': qtde})
            if itemCreated:
                response += "Created item {} {}\n".format(numero, codigoProduto)
            else:
                response += "Item {} {} exists, updating\n".format(numero, codigoProduto)
        except IntegrityError:
            response_err += "IntegrityError in adding or updating {} \n".format(numero)
    else:
        response = "ALL OK\n" + response
        Atualizado.atualizar('venda')
        
    return response_err + "</pre>" + response
