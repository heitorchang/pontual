from django.conf.urls import url

from . import views

app_name = 'records'

"""
Records URLs add and update data from Excel files
"""

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^produto/$', views.produto, name='produto'),
    url(r'^estoque/$', views.estoque, name='estoque'),
    url(r'^site/$', views.site, name='site'),
    url(r'^estoque/manual/$', views.estoque_manual, name='estoque_manual'),
    url(r'^pedido/$', views.pedido, name='pedido'),
    url(r'^venda/$', views.venda, name='venda'),
    url(r'^pedidouniao/$', views.pedidoUniao, name='pedidoUniao'),
    url(r'^vendauniao/$', views.vendaUniao, name='vendaUniao'),
    url(r'^compra/$', views.compra, name='compra'),
    url(r'^itemcompra/$', views.itemCompra, name='itemCompra'),
    url(r'^chegando/$', views.chegando, name='chegando'),
]
