from django.contrib import admin
from . import models

class ProdutoAdmin(admin.ModelAdmin):
    search_fields = ['codigo', 'nome']

class ClienteAdmin(admin.ModelAdmin):
    search_fields = ['codigo', 'nome']

class ChegandoAdmin(admin.ModelAdmin):
    search_fields = ['produto__codigo', 'nome']
                     
admin.site.register(models.Produto, ProdutoAdmin)
admin.site.register(models.Vendedor)
admin.site.register(models.Cliente, ClienteAdmin)
admin.site.register(models.Chegando, ChegandoAdmin)
admin.site.register(models.Pedido)
admin.site.register(models.ItemPedido)
admin.site.register(models.Compra)
admin.site.register(models.ItemCompra)
admin.site.register(models.PedidoUniao)
admin.site.register(models.ItemPedidoUniao)
admin.site.register(models.Atualizado)
